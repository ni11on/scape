package com.nilton.scape;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PagerAdapter extends FragmentPagerAdapter {

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return UserFragment.newInstance();

            case 1:
                return FriendsFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


}
